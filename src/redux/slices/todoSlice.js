import { createSlice } from "@reduxjs/toolkit";
import { fetchTodos } from "../actions/fetchTodos";

const initialState = {
  entity: [],
  loading: "idle",
  error: null,
};

const todoSlice = createSlice({
  name: "todos",
  initialState,
  reducers: {
    addTodo: (state, action) => {
      // return [...state, action.payload];
      state.entity = [...state.entity, action.payload];
    },
    changeStatus: (state, action) => {
      const newTodos = state.entity.map((todo) => {
        if (todo.id === action.payload.id) {
          return {
            ...todo,
            completed: !todo.completed,
          };
        }
        return todo;
      });
      // return newTodos;
      state.entity = newTodos;
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(fetchTodos.pending, (state, action) => {
        state.loading = "pending";
      })

      .addCase(fetchTodos.fulfilled, (state, action) => {
        // return action.payload;
        state.entity = action.payload;
        state.loading = "fulfilled";
      })

      .addCase(fetchTodos.rejected, (state, action) => {
        state.loading = "rejected";
        state.error = action.error;
      });
  },
});

// actions
export const { addTodo, changeStatus } = todoSlice.actions;
// selector
export const selectorTodo = (state) => {
  const {
    filter,
    todos: { entity },
  } = state;
  if (filter === "complete") return entity.filter((todo) => todo.completed);
  if (filter === "incomplete") return entity.filter((todo) => !todo.completed);
  return entity;
};

export const selectLoading = (state) => state.todos.loading;
// reducer
export default todoSlice.reducer;
