import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { addTodo, selectLoading, selectorTodo } from "./redux/slices/todoSlice";
import ListItem from "./Components/ListItem";
import { setFilter } from "./redux/slices/filterSlice";
import { fetchTodos } from "./redux/actions/fetchTodos";

function App() {
  const dispatch = useDispatch();
  const todos = useSelector(selectorTodo);
  const loading = useSelector(selectLoading);

  const [inputValue, setInputValue] = useState("");

  const handleSubmit = (event) => {
    event.preventDefault();

    const todo = {
      id: Math.random().toString(36),
      title: inputValue,
      completed: false,
    };

    dispatch(addTodo(todo));
    setInputValue("");
  };

  return (
    <div>
      <form onSubmit={handleSubmit}>
        <input
          value={inputValue}
          onChange={(event) => setInputValue(event.target.value)}
        />
      </form>
      <button onClick={() => dispatch(setFilter("all"))}>Show TODOS</button>
      <button onClick={() => dispatch(setFilter("complete"))}>Complete</button>
      <button onClick={() => dispatch(setFilter("incomplete"))}>
        Incomplete
      </button>
      <button onClick={() => dispatch(fetchTodos())}>Fetch</button>
      {loading === "pending" ? <p>Loading...</p> : null}
      {loading === "rejected" ? <p>Something was wrong</p> : null}
      <ul>
        {todos.map((todo) => (
          <ListItem key={todo.id} todo={todo} />
        ))}
      </ul>
    </div>
  );
}

export default App;
