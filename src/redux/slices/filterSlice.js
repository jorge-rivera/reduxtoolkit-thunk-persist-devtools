import { createSlice } from "@reduxjs/toolkit";

const initialState = "all";

const filterSlice = createSlice({
  name: "filter",
  initialState,
  reducers: {
    setFilter: (_, action) => {
      return action.payload;
    },
  },
});

// actions
export const { setFilter } = filterSlice.actions;
// reducer
export default filterSlice.reducer;
