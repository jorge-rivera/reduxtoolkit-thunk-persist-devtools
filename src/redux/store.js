import { configureStore } from "@reduxjs/toolkit";
import rootReducer from "./slices/rootReducer";
import storage from "redux-persist/lib/storage";
import { persistReducer, persistStore } from "redux-persist";

const isDevelopment = process.env.NODE_ENV !== "production" ? true : false;

const persistConfig = {
  key: "root",
  storage,
  whitelist: ["todos", "filter"],
};
const persistedReducer = persistReducer(persistConfig, rootReducer);

export const store = configureStore({
  reducer: persistedReducer,
  // Opcion 1
  // middleware: (getDefaultMiddleware) =>
  //   getDefaultMiddleware({ serializableCheck: false }),
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware({
      serializableCheck: {
        ignoredActions: ["persist/PERSIST"],
      },
    }),

  devTools: isDevelopment,
});

export const persistor = persistStore(store);
