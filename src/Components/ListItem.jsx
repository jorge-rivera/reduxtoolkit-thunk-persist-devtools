import { useDispatch } from "react-redux";
import { changeStatus } from "../redux/slices/todoSlice";

const ListItem = ({ todo }) => {
  const dispatch = useDispatch();

  const handleChangeStatus = (todo) => {
    dispatch(changeStatus(todo));
  };

  return (
    <li
      key={todo.id}
      style={{ textDecoration: todo.completed ? "line-through" : "none" }}
      onClick={() => handleChangeStatus(todo)}
    >
      {todo.title}
    </li>
  );
};

export default ListItem;
